#BchunkX

BchunkX is an Mac OS X app to combine bin/cue files to iso files.

##Requirements
- Mac OS X 10.7 or later

##Download
[Download](https://bitbucket.org/iteufel/bchunkx/downloads)

##Please Read
[Open an app from an unidentified developer](https://support.apple.com/kb/PH18657?locale=en_US)